#
# Copyright 2021, Ishaan Arora <ishaanarora1000@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Some useful helper methods for tests."""

from contextlib import contextmanager

# When upgrading to Django 3.0 use view.setup() instead of this
# helper function - https://code.djangoproject.com/ticket/20456
def setup_view(view, request, *args, **kwargs):
    """
    Mimic ``as_view()``, but return view instance.
    Helps to run unit tests on specific methods of a view.
    """
    view.request = request
    view.args = args
    view.kwargs = kwargs
    return view

@contextmanager
def clear_cache(cache):
    """Clear the cache even if any errors are thrown."""
    cache.clear()
    yield
    cache.clear()